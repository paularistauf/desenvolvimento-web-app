import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const LoginForm = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 40%;
  height: 390px;
  padding: 0 84px;
`;

export const SignUpForm = styled(LoginForm)`
  width: 60%;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const InputGroup = styled.form`
  display: flex;
  flex-direction: column;
  margin-top: 36px;
`;

export const InputComponent = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 30%;
  margin-bottom: 24px;
`;

export const InputLabel = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 14px;
  display: flex;
`;

export const Input = styled.input`
  margin-top: 8px;
  margin-right: 12px;
  padding: 12px;
  border: none;
  width: 100%;
`;

export const Footer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: flex-end;
  margin-top: 24px;
`;

export const InputGroupInLine = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const AlertMessage = styled.div`
  position: absolute;
  bottom: 24px;
  width: 100%;
`;

export const LoginMessage = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 24px;
  color: blue;
  margin-top: 4px;
`;



export const H2 = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 36px;
  line-height: 24px;
  display: flex;
  align-items: flex-start;
  text-align: flex-start;
  letter-spacing: 0.15px;
  color: #03396B;
`;

export const Button = styled.button`
  margin-top: 24px;
  width: 100px;
  height: 42px;
  border: none;
  border-radius: 8px;
  background: #03396B;
  color: white;
  cursor: pointer;
`

export const SignInLink = styled.div`  
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 24px;
  display: flex;
  margin-top: 12px;

  a{
    text-decoration: underline;
    color: #03396B;
    margin-left: 4px;
  }
`

export const UserData = styled.div`
padding: 12px;
`;