import React, { useEffect, useState } from "react";
import { Container, H2, InputGroup, UserData } from "../styles";
import firebase from "../../data/Firebase";
import { INetworkUser } from "../../contexts/auth";

export const Home = () => {
  const [networkUser, setNetworkUser] = useState<INetworkUser>();

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async(user) => {
      if(user){
        var uid = user.uid;

        await firebase.firestore().collection("usuario").doc(uid).get()
        .then((response) =>{
          console.log("response",response?.data())
   
         setNetworkUser({name: response?.data()?.name , lastname: response?.data()?.lastname, birthday: response?.data()?.birthday})

        })
      }
    })
  }, [])

  return <Container>
    <InputGroup>
    <H2>Usuário</H2>
    <UserData>
      Nome: {networkUser?.name ?? "oo"}
      <br/>
      Sobrenome: {networkUser?.lastname ?? "oo"}
      <br/>
      Data de nascimento: {networkUser?.birthday ?? "oo"}
    </UserData>
    </InputGroup>
  </Container>
}