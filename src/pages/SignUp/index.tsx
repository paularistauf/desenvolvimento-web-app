import React, { useState } from "react";
import { Button, H2, InputGroup, Container, Input, Row, SignInLink, SignUpForm, InputComponent,
  InputLabel } from "../styles";
import firebase from "../../data/Firebase";


export const SignUp = () => {
  const [name, setName] = useState<string>("");
  const [lastname, setLastname] = useState<string>("");
  const [birthday, setBirthday] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  const handleSubmit = async(e:React.FormEvent<HTMLFormElement>) =>{
    e.preventDefault();
    setLoading(true);

    try{
      await firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((response) => {
        firebase.firestore().collection("usuario").doc(response.user?.uid).set({
          name: name,
          lastname: lastname,
          birthday:birthday
        })
      }).then(() => {window.location.href = "./login";});
    }catch(error){
      console.error("Não foi possível concluir o cadastro", error)
    }finally{
      setLoading(false);
    }
  }

  return (
    <Container>
      <SignUpForm>
        <H2>Cadastro</H2>
        <InputGroup onSubmit={(e) => handleSubmit(e)}>
          <Row>
            <InputComponent>
              <InputLabel>Nome</InputLabel>
              <Input
                type="text"
                id="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </InputComponent>
            <InputComponent>
              <InputLabel>Sobrenome</InputLabel>
              <Input
                type="text"
                id="lastname"
                value={lastname}
                onChange={(e) => setLastname(e.target.value)}
              />
            </InputComponent>
            <InputComponent>
              <InputLabel>Data de nascimento</InputLabel>
              <Input
                type="date"
                id="birthday"
                value={birthday}
                onChange={(e) => setBirthday(e.target.value)}
              />
            </InputComponent>
            
          </Row>
            <InputComponent>
              <InputLabel>E-mail</InputLabel>
              <Input
                type="text"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputComponent>
            <InputComponent>
              <InputLabel>Senha</InputLabel>
              <Input
                placeholder='Senha'
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </InputComponent>
          {loading ? (
              <Button
              type='submit'
                disabled
              >
                Loading...
              </Button>
            ) : (
              <Button
              type='submit'
              >
                Salvar
              </Button>
            )}
            <SignInLink>Possue uma conta? <a href="/login">Faça login.</a></SignInLink>
        </InputGroup>
      </SignUpForm>
    </Container>
  )
}