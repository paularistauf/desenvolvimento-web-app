import React, {  useState } from "react";
import { Button, H2, InputGroup, Container, LoginForm, Input, LoginMessage, SignInLink } from "../styles";
import { useAuth } from "../../hooks/useHooks";


export const Login = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const { handleAuth, errorMessage } = useAuth();

  const handleSubmit = async(e:React.FormEvent<HTMLFormElement>) =>{
    e.preventDefault();
    setLoading(true);

    try{
      await handleAuth(email, password);
    }catch(err){
      console.log(err);
    }
    finally{
      setLoading(false);
    }
  }
  
  return (
    <Container>
      <LoginForm>
        <H2>Login</H2>
        <InputGroup onSubmit={(e) => handleSubmit(e)}>
          <Input
            placeholder='Email'
            type="text"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Input
            placeholder='Senha'
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          {!!errorMessage.length && <LoginMessage >{errorMessage}</LoginMessage>}
         
            {loading ? (
                <Button
                type='submit'
                  disabled
                >
                  Loading...
                </Button>
              ) : (
                <Button
                type='submit'
                >
                  Acessar
                </Button>
              )}
              <SignInLink>Não possue uma conta? <a href="/sign-up">Crie uma.</a></SignInLink>
         
        </InputGroup>

      </LoginForm>
    </Container>
  );
}