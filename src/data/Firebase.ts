import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth"

const firebaseConfig = {
  apiKey: "AIzaSyAXANjqZbwma7iVtso00-L11AOuSl9cuE4",
  authDomain: "desenvolvimento-web-app.firebaseapp.com",
  projectId: "desenvolvimento-web-app",
  storageBucket: "desenvolvimento-web-app.appspot.com",
  messagingSenderId: "802510232082",
  appId: "1:802510232082:web:4bc25019a1608f77f896cb"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default firebase;