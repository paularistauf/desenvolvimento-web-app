import React, { PropsWithChildren } from 'react';

import AuthProvider from './auth';

export const ContextProvider : React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <AuthProvider>
      {children}
    </AuthProvider>
  );
}
