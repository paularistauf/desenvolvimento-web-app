import { INetworkUser } from ".";

export interface IAuthContext {
  networkUser: INetworkUser | undefined;
  handleAuth: (user: string, passoword: string) => Promise<void>;
  errorMessage: string;
}
