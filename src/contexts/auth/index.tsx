import React, { createContext, PropsWithChildren, useState } from 'react';
import { IAuthContext } from './types';
import firebase from "../../data/Firebase";


export const AuthContext = createContext<IAuthContext>({} as IAuthContext);

export interface INetworkUser{
  name: string,
  lastname: string,
  birthday:string,
}

const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
  
  const [errorMessage, setErrorMessage] = useState('');
  const [networkUser] = useState<INetworkUser>();
  
  const handleAuth = async(email: string, password: string) => {
     await firebase.auth().signInWithEmailAndPassword(email, password)
    .then(()=>{
      window.location.href = "./home";})
    .catch((error) => {
      console.log(error);
      setErrorMessage(error.message);
    });
    
  };

  return (
    <AuthContext.Provider value={{ networkUser,  handleAuth, errorMessage }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
