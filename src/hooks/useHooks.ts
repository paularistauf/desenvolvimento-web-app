import { useContext } from "react";
import { AuthContext } from "../contexts/auth";

function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within an Provider.');
  }

  return context;
}
export { useAuth };