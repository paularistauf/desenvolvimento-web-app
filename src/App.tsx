import React from 'react';
import './App.css';
import Routes from './routes';
import { BrowserRouter } from 'react-router-dom';
import {ContextProvider} from './contexts';

function App() {
  return (
    <div>
    <ContextProvider>
      <BrowserRouter>
        <Routes/>
      </BrowserRouter>
    </ContextProvider>
    </div>
  )
}

export default App;
