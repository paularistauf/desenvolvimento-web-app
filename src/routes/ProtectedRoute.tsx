import React, { PropsWithChildren} from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from '../hooks/useHooks';

const PrivateRoute: React.FC<PropsWithChildren> = ({ children, ...rest }) => {
  const { networkUser } = useAuth();
  

  return (
    <Route
      {...rest}
      render={({ location }) =>
      !networkUser ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
